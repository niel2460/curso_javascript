//formas de importar y exportar modulos
//1 CommonJS - requiere
//2. Import ES6 - import

// console.log(module)
//
const moduloMatematicas = require("./modulos/matematicas.js");
const factorial = moduloMatematicas.factorial;
const { multiplica, eleva } = require("./modulos/matematicas.js");

const fact = factorial(5);
console.log(fact);

const sum = moduloMatematicas.suma(12, 90);
console.log(sum);

console.log(multiplica(3, 3));
console.log(eleva(3, 3));
