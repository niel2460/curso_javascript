//heremcia
class Persona {
  constructor(nombre, edad, isDeveloper) {
    this.#nombre = nombre;
    this.#edad = edad;
  }

  saludo() {
    console.log(`Hellow mi nombre es ${nombre}, tengo ${edad} anios.`);
  }
}
class Desarrollador extends Persona {
  constructor(nombre, edad, lenguaje) {
    super(nombre, edad); //llamar el metodo constructor de Persona y darle los valores
    this.lenguaje = lenguaje;
  }
  saludo() {
    //override
    super.saludo();
    console.log("Hola souy Desarrollador");
  }
}

const nuevodev = new Desarrollador("Niel", 34, "JavaScript");
console.log(nuevodev);
nuevodev.saludo();

//polymorfismo varias formas
