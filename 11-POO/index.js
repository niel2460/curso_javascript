const persona = {
  nombre: "Niel",
  edad: 34,
  isDeveloper: true,
  saludo: function () {
    console.log("hola");
  },
};

persona.saludo();

const otra_persona = {
  nombre: "Alex",
  edad: 20,
  isDeveloper: false,
  saludo: function () {
    console.log("hola");
  },
};
otra_persona.saludo();

const creaPersona = (nombre, edad, isDeveloper) => {
  return {
    nombre: nombre,
    edad: edad,
    isDeveloper: isDeveloper,
    saludo: function () {
      console.log("hellow");
    },
  };
};

const nueva_persona = creaPersona("juan", 23, true);
console.log(nueva_persona.saludo());
