class Persona {
  nombre;
  edad;
  isDeveloper;

  constructor(nombre, edad, isDeveloper) {
    this.nombre = nombre;
    this.edad = edad;
    this.isDeveloper = isDeveloper;
  }

  saludo() {
    console.log(`Hellow mi nombre es ${nombre}, tengo ${edad} anios.`);
  }
}

const nueva_persona = new Persona("Niel", 34, true);
console.log(nueva_persona);
nueva_persona.saludo();

let numero = 60; //inicializar
let persona2 = new Persona("Maria", 34, false); //instanciar
console.log(typeof persona2);
console.log(persona2 instanceof Persona);

//instanceof -> similiar a typeof pero para clases
