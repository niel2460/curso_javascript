class Persona {
  //atributo privada -> #
  //privada solo se puede acceder desde dentro de la clase
  #nombre;
  #edad;

  //atributo protegido -> _
  //protegido solo se puede acceder desde debtro de la clase y desde las clasces decendientes
  _isDeveloper = true;

  constructor(nombre, edad, isDeveloper) {
    this.#nombre = nombre;
    this.#edad = edad;
    this.#isDeveloper = isDeveloper;
  }

  saludo() {
    console.log(`Hellow mi nombre es ${nombre}, tengo ${edad} anios.`);
  }

  obtenerNombre() {
    //funcion getter -> nos permite acceder de forma controlada a algun atributo protegido
    return this.#nombre;
  }
  getEdad() {
    return this.#edad;
  }
  setEdad(nuevaedad) {
    this.#edad = nuevaedad;
  }
}

const persona = new Persona("Niel", 80);
console.log(persona);
console.log(persona.obtenernombre);
persona.saludo();
console.log(persona.obtenerEdad);
console.log(persona._isDeveloper);

//Getter -> metodos que nos permiten obtener atributos/metodos privados o protegidos
const edad = persona.getEdad();
console.log(edad);
//Setter -> metodos que nos permitem cambiar el valor de algunos de  los atributos privados o protegidos
persona.setEdad(34);
console.log(persona.getEdad());
