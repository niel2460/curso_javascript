function funcionFibonacci(num){
    if(num===1) return [1]
    
    if(num===2) return [1,1]
    
    let res = [1,1]

    for(let i=2;i<num;i++){
        res.push(res[i-1]+res[i-2])
    }
    return res
}

console.log(funcionFibonacci(6))