const boton = document.querySelector("#btn");

console.log(boton);

boton.addEventListener("click", () => {
  // console.log("click");
  // alert("se ha hecho clck")
  // confirm("estas deacuerdo?") && console.log("Ok")
  // confirm("estas deacuerdo?")
  // ? console.log("Ok")
  // : console.log("No");
  //
  const respuesta = confirm("seguro?");
  if (respuesta) {
    console.log("estas deacuerdo");
  } else {
    console.log("No estas deacuerdo");
  }
});

const botonInfo = document.querySelector("#info");

botonInfo.addEventListener("click", () => {
  const nombre = prompt("Cual es tu nombre:");
  if (nombre) {
    console.log("Tu nombre es: " + nombre);
  } else {
    console.log("No introducistes nada!");
  }
});

const lista = [
  {
    nombre: "Niel",
    edad: 34,
  },
  { nombre: "alexander", 
    edad: 20
  },
  {
    nombre: "william",
    edad: 23,
  },
];

// console.log(lista)
console.table(lista)
