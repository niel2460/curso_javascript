//$(selector).accion()
//
// $("li").hide()
//
//  $(document).ready(() =>{

//  })

$(() => {
  //selectores
  //id = "el-1" => "#el-1"
  //class="el-1" => ".el-1"
  // $("h1").hide();
  // $("#el-1").hide();

  $(".hide-btn").click(() => {
    console.log("ocultando");
    // $("h1").hide();
    $("h1").fadeOut();
  });

  $(".show-btn").click(() => {
    console.log("mostrando");
    // $("h1").show();
    $("h1").fadeIn();
  });

  $("li").dblclick(() => {
    console.log("cambiando color a h1");
    // $("h1").show();
    $("h1").css({ color: "red" });
  });

  $(".new-element").click(() => {
    // $("ul").append("<li>New element</li>")
    $("ul").prepend("<li>New element</li>");
  });

  $("li").mouseenter((elem) => {
    elem.target.style.color = "blue";
    console.log("Ha entrado el raton (hover)");
  });

  $("li").mouseleave((elem) => {
    elem.target.style.color = "black";
  });
});
