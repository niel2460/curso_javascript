function initMap() {
  const posicion = {
    lat: -25.362,
    lng: 131.044,
  };
  const mapp = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: posicion,
  });
  markers.push(
    new google.maps.Marker({
      position: {
        lat: 16.32394851522728,
        lng: -86.53545503583968,
      },
      map,
      title: "Roatan",
    })
  );
  markers.push(
    new google.maps.Marker({
      position: {
        lat: 14.44777827871021,
        lng: -87.64204821442745,
      },
      map,
      title: "Comayagua",
    })
  );
  markers.push(
    new google.maps.Marker({
      position: {
        lat:14.851045123717103,
        lng:-89.14306924984396,
      },
      map,
      title: "Copan Ruinas",
    })
  );
}
