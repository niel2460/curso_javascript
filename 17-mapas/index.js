function initMap() {
  const posicion = {
    lat: -25.362,
    lng: 131.044,
  };
  const mapp = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: posicion,
  });

  const marker = new google.maps.marker({
    position: posicion,
    map,
    title: "Posicion Inicial",
  });

  geoPosicion();
}

//obtener la geolocalizacion
//marker.setPosition{lat,lng}
function geoPosicion() {
  if (navigator.geolocation) {
    const geoLoc = navigator.geolocation;
    const options = { timeout: 60000 };
    const watchPos = geoLoc.watchPosition(centraMapa, onError, options);
  } else {
    alert("tu navegador no soporta geolocalizacion");
  }
}

function centraMapa(position) {
  const nuevaPos = {
    lat: position.coords.latitude,
    lng: position.coords.longitude,
  };
  console.log(nuevaPos);
  marker.setPosition(nuevaPos);
  map.center(nuevaPos);
}

function onError(error) {
  console.log("Se ha producido un error")
  console.log(error);
}
