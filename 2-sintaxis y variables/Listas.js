//Listas -- array o arreglo
const lista = [1, "Hola", true, undefined, null];
const lista2 = new array(1, "Hola", true, undefined, null);
const lista3 = new array(3);
lista3[0] = "Soy el primer elemento, index 0";
const lista4 = [lista, lista2, lista3];

console.log(lista);
console.log(lista2);
console.log(lista3);
console.log(lista4);

//Objetos
const movil = {
  altura: 10,
  anchura: 5,
  marca: "Xiaomi",
  iswhite: false,
  constacto: ["Niel", "Alexander", "Jafeth", "William"],
  tarjeta: {
    marca: "Scandisk",
    almacenamiento: 64,
  },
  "altura-tarjeta": 4,
};

movil.anio = 2019;
movil.marca = "Samsung";

console.log(movil.tarjeta.anio);

//Fechas
//Librerias de aporo Moment.js
const ahora = new Date();
console.log(ahora);

const fecha_milis = new Date(10); //Utilizando los milisegundos
console.log(fecha_milis);

const fecha_cadena = new Date("march 25 2020");
console.log(fecha_cadena);

const fecha_valores = new Date(2022, 0, 15);
console.log(fecha_valores);

const dia = ahora.getDay();
const mes = ahora.getMonth();
const anio = ahora.getFullYear();

console.log(dia, mes, anio);
