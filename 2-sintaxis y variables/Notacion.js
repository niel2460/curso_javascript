//notacion
// ; --sirva para delimitar el final de una linea
const b = 4;

// . --se utiliza en los objetos para acceder a los atributos
// movil.anchura

// [] --se utiliza en listas, arreglos y array
const ar = [1, 2, 3, 4];
console.log(ar[2]);

// () --se utiliza en las funciones
function suma(a, b) {
  // muestra la funcion
}

// {} --llaves para objetos, funciones y estructuras de control
const movil = {
  anchura: 5,
  altura: 10,
};

if (true) {
  //todo lo que esta entre llaves

  const constante2 = "hola";
}
