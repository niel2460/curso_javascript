//----tipos de datos
//----Primitivos
//number
4;

//string
"hola mundo";
'hola mundo';
`hola mundo`;

//boleanos
true;
false;

//nulo y undefined
null;
undefined;

// null, undefined, false, 0
if (true) {
 console.log("cumple")
}else{
  console.log("no cumple")
}
