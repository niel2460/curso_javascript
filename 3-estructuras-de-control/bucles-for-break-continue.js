//uso de break y continue
let lista = [1, 2, 3, 4, 5, 6, 7, 8, 9];

for (let i = 0; i < lista.length; i++) {
  if (lista[i] == 3) {
    continue;
  }
  console.log(lista[i]);

  if (lista[i] > 5) {
    break;
  }
}

//cual es el ambito de un bucle
// console.log(k);
// console.log(i);
// console.log(j);

//break continue
//labels

let unidades = 0;
let decenas = 0;

bucleDecenas: while (true) {
  console.log(`El numero actual es: ${decenas}${unidades}`);

  bubleUnidades: while (true) {
    unidades++;
    if (unidades == 10) {
      break;
      unidades = 0;
    }
  }
  decenas++;
  if (decenas == 2) {
    console.log(`El numero actual es: ${decenas}${unidades}`);
    break;
  }
}
