//bucles

for (let i = 0; i < 10; i++) {
  //esto es un bucle
  console.log(i);
}

let lista = [1, 4, 6, 2, 3, 7, 10, 12, 800]
for (let i = 0; i < lista.length; i++) {
  console.log(lista[i]*2);
  
}

//estructura for...of
for (let valor of lista) {
  console.log(valor)
  
}

//forEach
lista.forEach(valor => {
  console.log(valor)
});

//for..in
let persona ={
  nombre : "Niel",
  apellido : "Molina",
  edad : 34,
  isDeveloper : true,
}
console.log(persona.nombre)

let prop = "edad"
console.log(persona[prop])

for (let propiedad in persona) {
  console.log(propiedad)
  console.log(persona)
  
}
