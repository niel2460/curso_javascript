//bucles while
//
let i = 0;
let max = 10;

while (i < max) {
  console.log(i);
  i++;
}


//bucle do while 
//
i = 0;
do {
  i++;
  console.log(i);
} while (i < max);
