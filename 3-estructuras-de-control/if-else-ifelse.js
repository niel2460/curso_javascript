let nota = 5;
if ((nota = 5)) {
  console.log("Enhorabuena, has obtenido un sobresaliente");
} else if ((nota = 4)) {
  console.log("Haz obtenido una nota muy buena");
} else if ((nota = 3)) {
  console.log("Haz obtenido una nota suficiente");
} else if ((nota = 2)) {
  console.log("Haz aprovado por poco");
} else if ((nota = 1)) {
  console.log("No has estudiado nada, trabaja un poquito mas para la proxima");
} else {
  console.log("Error, ingrese una nota entre 1 y 5");
}
