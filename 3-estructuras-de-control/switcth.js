let nota = 5;

switch (nota) {
  case 5:
    console.log("Enhorabuena, has obtenido un sobresaliente");
    break;
  case 4:
    console.log("Haz obtenido una nota muy buena");
    break;
  case 3:
    console.log("Haz obtenido una nota suficiente");
    break;
  case 2:
    console.log("Haz aprovado por poco");
    break;
  case 1:
    console.log(
      "No has estudiado nada, trabaja un poquito mas para la proxima"
    );
    break;
  default:
    console.log("Error, ingrese una nota entre 1 y 5");
    break;
}
