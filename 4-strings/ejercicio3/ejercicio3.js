let nombre = "Niel";
let apellido = "Molina";
let estudiante = nombre.concat(" ").concat(apellido);
console.log(estudiante);

let estudianteMayus = estudiante.toUpperCase();
console.log(estudianteMayus); 

let estudianteMinus = estudiante.toLowerCase();
console.log(estudianteMinus);

let numero_letras = estudiante.length;
console.log(numero_letras);

let primera_letra = estudiante.substring(0,1);
console.log(primera_letra);

let ultima_letra = estudiante.substring(estudiante.length -1, estudiante.length);
console.log(ultima_letra);

let eliminar_espacios = estudiante.replace(/ /g, "");
console.log(eliminar_espacios);

let variable_contenida = estudiante.includes('Niel')
console.log(variable_contenida)
