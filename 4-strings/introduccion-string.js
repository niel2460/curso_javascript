let str_sng = "Hola soy un texto con comillas simples";
let str_dbl = "Hola soy un texto con comillas dobles";

console.log(str_sng);
console.log(str_dbl);

let str_comillas = 'El otro dia me dijo literalmente "ve a sacar la basura"';
let str_comillas_simples =
  'El otro dia me dijo literalmente "ve a sacar la basura"';

let str_comillas_dobles =
  "El otro dia me dijo literalmente 've a sacar la basura'";

console.log(str_comillas);
console.log(str_comillas_simples);
console.log(str_comillas_dobles);

//comillas invertidas
let str_backticks = `hola esto es un string con str_backticks`;

let nombre = "Niel";
let saludo = `Hola, ${nombre} bienvenido`;

console.log(saludo);
