//metodos mas utilizado con cadenas de caracteres
//como obtener la longitud de un string
let str = "Hola soy un string"
console.log(str.length)

//obtener partes de cadena de caracteres
//slice() substring() substr()

let slice_str = str.slice(0,10)
console.log(slice_str)

let substring_str = str.substring(5,10)
console.log(substring_str)

let substr_str = str.substr(5,10)
console.log(substr_str)

let cadena = "Hola mi nombre Niel"
console.log(cadena)
console.log(cadena.replace('Niel', 'Alexander'))


let texto_largo = "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat. Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat."

//solo reemplaza la primera coincidencia
console.log(texto_largo.replace('lorem','cinco'))

//al utilizar la exprecion regular /g (global) reemplaza todas las intancias
console.log(texto_largo.replace(/los/g, 'cinco'))
