//metodos de cadenas de texto
//
let input = "eScorPio";
let db = "escorpio";

console.log(input == db);
console.log(input.toLowerCase());
console.log(input.toUpperCase());
console.log(input.toLowerCase() == db.toLowerCase());
console.log(input.toUpperCase() == db.toUpperCase());

//formas de concatenar 2 cadenas de caracteres
let str1 = "Hola soy la primera cadena";
let str2 = "y yo soy la segunda cadena";

console.log(str1.concat(" ", str2));
console.log(str1 + " " + str2);

//eliminar espacios al inicio y al final
let str3 = "    Hola soy un string con espacios al final.   ";
console.log(str3.length);
//trim
console.log(str3.trim().length);
console.log(str3.trimStart().length);
console.log(str.trimEnd().length);

//obtener el caracter que hay en cierta pocicion
let str4 = "Hola soy un el string numero 4";

console.log(str4.charAt(6));

//obtener la posicion de una oalabra dentro de una cadena de caracteres
let str5 = "Hola soy Niel y me gusta el football";
console.log(str5.indexOf("Niel"));
console.log(str5.charAt(9));
