let altura_cm = 180;
console.log(altura_cm);

let altura_m = 1.8;
console.log(altura_m);

let peso_kg = 85.58;
console.log(peso_kg);

let altura_redondeo = Math.ceil(altura_m);
console.log(altura_redondeo);

let peso_redondeo = Math.floor(peso_kg);
console.log(peso_redondeo);

let valor_maximo = Number.MAX_VALUE + 1 == Number.MAX_VALUE;
console.log(valor_maximo);
