//operador valueOf() -Obtener valores numericos a partir de literales
let a = 2;
let b = new Number(3);

console.log(a);
console.log(b);
console.log(a + b);
console.log(a.valueOf() + b.valueOf());

console.log(b.valueOf());

let str = new String("Hola soy un string");
console.log(str);
console.log(srt.valueOf());

//Nan (not a number) - Infinity
let n = Number("Adios");
console.log(n);
console.log(isNaN(n));

let numerador = 19;
let divisor = 0;

console.log(numerador / divisor);

let divisor_2 = null;
console.log(numerador / divisor_2);

//como convertir los string a numeros
let numero = "5.89";
let num2 = 17.2;

console.log(typeof numero);
console.log(numero + num2);

console.log(typeof numero + num2);

console.log(parceInt(numero));
console.log(parseFloat(numero));

let num3 = 4;

console.log(parceInt(num3));
console.log(parseFloat(num3));

//numeros ehexadecimales base 16
let numHex = "0x3a567";
console.log(parseInt(numHex));

//obtener los valores maximos y minimos en JavaScript
let min_presision = Number.EPSILON;
let min_valor_JS = Number.MIN_VALUE;
let max_valor_JS = Number.MAX_VALUE;

console.log(min_presision);
console.log(min_valor_JS);
console.log(max_valor_JS);

//elevar un numero a la potencia x
console.log(2 ** 100)
