//operacoines aritmeticas
let a = 3.5;
let b = 4.8;

//suma
console.log(a + b);
//resta
console.log(a - b);
//multiplicacion
console.log(a * b);
//division
console.log(a / b);

//representacion de los numeros en cadenas de texto
console.log(typeof a);
let a_s = a.toString();

console.log(a_s);
console.log(typeof a_s);

//redondeo de numeros decimales
let c = 3.3;
let d = c * 3;

//.toFixed(X) limita el numero de decimales al valor X;
console.log(d);
console.log(d.toFixed(4));
console.log(typeof d.toFixed(4));

let e = 1830.123456789;
let f = 22135563215;
console.log(e.toFixed(2));
console.log(f.toFixed(2));

//toPrecision(X) --limita el numero de cifras significativas
console.log(e.toPrecision(7));
console.log(f.toPrecision(7));

console.log(typeof f.toPrecision(4));
