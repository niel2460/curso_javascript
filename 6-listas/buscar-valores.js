//busqueda de valores some()
const arrry = [3, 7, 2, 4, 7, 9, 42, 35, 7865, 23, -2];

const res = array.some((valor) => valor, -0);

console.log(res);

const existe = array.some((valor) => valor === 9);

console.log(existe);

const listaObjetos = [
  { nombre: "Leire", edad: 35 },
  { nombre: "alex", edad: 34 },
  { nombre: "william", edad: 23 },
  { nombre: "lucia", edad: 30 },
];

const existePersona = listaObjetos.some((persona) => persona.nombre === "alex");

console.log(existePersona);

//como obtener una lista a partir de un objeto iterable
const str = "Hola soy Niel";
console.log(str[5]);

const ar_str = Array.from(str);
console.log(ar_str);

const set = new set[(2, 3, "hola,4")]();
const ar_set = array.from(set);
console.log(ar_set);

const keys = array.key();
console.log(keys);

const ar_key = array.from(keys);
console.log(ar_key);
