//iterear o acceder a valores de una lista
const array = ["hola", 2, 5, 90, false, undefined];

//forma antigua (poco eficiente)
for (let i = 0; i < array.length; i++) {
  console.log(array[i]);
}

//forma mas eficiente ES6 forEach
let suma = 0;
const arrayNums = [3, 6, 2, 77, 2, 3, 93, 19];

arrayNums.forEach((valor) => {
  suma += valor;
  console.log(valor);
});
console.log(valor);

//busqueda de un valor dentro de una lista .find()
//quiero encontrar el elemento 90
const variable = array.find((valor) => {
  if (valor === 90) {
    return true;
  }
});
console.log(variable);

const listaObjetos = [
  { nombre: "Leire", edad: 35 },
  { nombre: "alex", edad: 34 },
  { nombre: "william", edad: 23 },
  { nombre: "lucia", edad: 30 },
];
const objeto1 = listaObjetos.find((o) => {
  if (o.nombre === "lucia") {
    return true;
  }
});
console.log(objeto1.edad);

const objeto2 = listaObjetos.find((o) => {
  return o.nombre === "miguel";
});
console.log(objeto2.edad);

const { edad } = listaObjetos.find((o) => o.nombre === "lucia");
console.log(edad);
