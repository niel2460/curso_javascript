// Una variable que contenga la lista de la compra (mínimo 5 elementos)
const lista_compra = ["arroz", "frijoles", "huevos", "queso", "harina"];
console.log(lista_compra);

//Modifica la lista de la compra y añádele "Aceite de Girasol"
lista_compra.push("aceite de girasol");
console.log(lista_compra);

//Vuelve a modificar la lista de la compra eliminando "Aceite de Girasol"
lista_compra.pop();
console.log(lista_compra);

//Una lista de tus 3 películas favoritas (objetos con propiedades: titulo, director, fecha)
const lista_peliculas = [
  { titulo: "Duro de matar", director: "Bruce Willies", anio: "1988" },
  { titulo: "Rocky", director: "John G. Avildsen", anio: "1976" },
  { titulo: "El camino del dragon", director: "Bruce Lee", anio: "1972" },
  { titulo: "Top Gun Maverick", director: "Joseph Kosinski", anio: "2022" },
];

//Una nueva lista que contenga las películas posteriores al 1 de enero de 2010 (utilizando filter)
const peliculas_nuevas = lista_peliculas.filter(
  (busqueda) => busqueda.anio > 2010
);
console.log(peliculas_nuevas);

//Una nueva lista que contenga los directores de la lista de películas original (utilizando map)
const directores = lista_peliculas.map((lista_peliculas) => {
  return lista_peliculas.director;
});
console.log(directores);

//Una nueva lista que contenga los títulos de la lista de películas original (utilizando map)
const titulos = lista_peliculas.map((lista_peliculas) => {
  return lista_peliculas.titulo;
});
console.log(titulos);

//Una nueva lista que concatene la lista de directores y la lista de los títulos (utilizando concat)
const directores_titulos = directores.concat(titulos);
console.log(directores_titulos);

//Una nueva lista que concatene la lista de directores y la lista de los títulos (utilizando el factor de propagación)
const directores_titulos_propagacion = [...directores, ...titulos];
console.log(directores_titulos);
