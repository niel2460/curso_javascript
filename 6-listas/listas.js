//como trabajar con listas(arrays, arreglos, vectores)
let var1 = "45";
let array = new aarray[(1, "hola", false, { id: 5 }, null, undefined, var1)]();

console.log(array);

//como acceder a los valores de un array atravez de su posicion
//array[indice]
console.log(array[0]);
console.log(array[1]);
console.log(array[2]);
console.log(array[3]);
console.log(array[4]);

//metodos para introducir nuevos valores en nuestros arrays
//push(), unshift()
array.push("final", 45, 100, false);
console.log(array);

//valores al principio
array.unshift("inicio", 87, 99);
console.log(array);

//metodos paa eliminar valores en nuestro array
//pop(), shit()
//valores al final
const array2 = [1, 3, "hola", false];

array.pop();
console.log(array2);

//valores al principio
array2.shift();
console.log(array2);

// metodos para eliminar , modificar  anadir vaors en nuestro array
const array3 = [1, 2, 3, 4, 5, 6];

//eliminar valores .splice(inidice, numvaloraeliminar)
array3.splice(2, 2);
console.log(array3);

//anadir valores .splice(inidice, 0, valores)
array2.splice(2, 0, "hola");
console.log(array3);

//modificar valores
array3.splice(2, 1, 3);
console.log(array3);
