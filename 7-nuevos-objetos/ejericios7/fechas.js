const fecha_actual = new Date(2023, 03, 13);
const fecha_cumpleanios = new Date(1988, 07, 26);

const hoy_es_mas_tarde = fecha_actual > fecha_cumpleanios;
console.log(hoy_es_mas_tarde);

const dia = fecha_cumpleanios.getDay();
const mes = fecha_cumpleanios.getMonth() + 1
const anio = fecha_cumpleanios.getFullYear()

console.log(dia);
console.log(mes);
console.log(anio);
