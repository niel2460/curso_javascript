const personas = {
  nombre: "Niel",
  apellido: "Molina",
  edad: 34,
  altura: 1.8,
  eresDesarrollador: true,
};

const edad = personas.edad;
console.log(edad);

const lista = [
  { ...personas },
  {
    nombre: "Jafeth",
    apellido: "Lopez",
    edad: 25,
    altura: 1.7,
    eresDesarrollador: true,
  },
  {
    nombre: "Grecia",
    apellido: "Ventura",
    edad: 30,
    altura: 1.65,
    eresDesarrollador: false,
  },
];

const lista_ordenada = lista.sort((a,b)=>b.edad -a.edad)

console.log(lista)
