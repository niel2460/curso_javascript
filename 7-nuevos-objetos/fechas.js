//trabajando con fechas
const fecha =new Date()

console.log(fecha)

//los meses inicializan en cero 0:enero y 11:diciembre
const fecha2 = new Date(1987,10,23)

console.log(fecha2)

const fecha3 = new Date(100000)
console.log(fecha3)

const fecha4 = new Date("october 12,1970 12:15:15")
console.log(fecha4)

console.log(fecha > fecha2)

const fecha5 = new Date(1987, 10,20,1,23,52,102)
console.log(fecha5)

console.log(fecha2 === fecha5)

console.log(fecha2.getTime() === fecha5.getTime())

//obtener el dia, el mes y el mes y anyo de una fecha
console.log(fecha2.getDate)

console.log(fecha2.getMonth()+1)

console.log(fecha2.getFullYear())

console.log(fecha2)

console.log(fecha2.toLocaleDateString())
