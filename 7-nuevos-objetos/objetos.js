// trabajando con objetos
//
const obj = {
  id: 4,
  nombre: "juan",
  apelido: "gonzales",
  isDeveloper: true,
  libros_favoritos: ["El metodo", "El codigo de la manifestacion"],
  "4-juegos": [1, 2, 3, 4],
};

console.log(obj.id);
console.log(obj["4-juegos"]);

const prop = "isDeveloper";
console.log(obj[prop]);

const obj2 = obj;
console.log(obj2);

obj2.nombre = "inigo";
console.log(obj.nombre);

let val1 = 4;
let val2 = val1;

val2 = 6;
console.log(val1);
console.log(val2);

const obj3 = [...obj];

obj3.nombre = "Alex";

console.log(obj.nombre);
console.log(obj3.nombre);

//como ordenar lista de objetos en funcion de una propiedad
const Listapelicula = [
  { titulo: "lo que el viento se llevo", anyo: 1939 },
  { titulo: "titanix", anyo: 1997 },
  { titulo: "moama", anyo: 2016 },
  { titulo: "efecto mariposa", anyo: 2004 },
  { titulo: "TED", anyo: 2012 },
];
console.log(Listapelicula);

//short muta el valor dellista original
Listapelicula.sort((a, b) => a.anyo - b.anyo);

console.log(Listapelicula)
