//que son las funciones y como se utilizan
const nom = "Niel";

saludar(nom);

function saludar(nombre) {
  console.log(`Hola ${nombre}`);
}

let nombre2 = "Juan";

console.log(nombre2);

despedir(nombre2);

function despedir(nombre2) {
  nombre = "Diego";
  console.log(nombre);
}

saludarPersona(persona);

let persona = { nombre: "Juan", apellido: "Gonzales" };

function saludarPersona(objeto) {
  objeto.apellido = "Villar";
  console.log(`Hola ${objeto.nombre} ${objeto.apellido}`);
}

//////
function imprimirNumero(numero = 7) {
  //parametros por defecto
  console.log(numero);
}
imprimirNumero();
imprimirNumero(9);

//////
function imprimir(...parametros) {
  console.log(parametros);
}
imprimir(1, 3, 9, 2, "hola", { id: 9 });

////
function suma(...nums) {
  console.log(nums.reduce((a, b) => a + b));
}
const s = suma(1, 2, 3, 4, 9, 15);

console.log(s);

/////
function multiplicar(a = 0, b = 0) {
  return a * b;
}

console.log(multiplicar(4, 9));
