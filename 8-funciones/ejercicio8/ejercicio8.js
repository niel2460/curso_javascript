function siempre_verdadero() {
  return true;
}

console.log(siempre_verdadero());

async function miPromesa() {
  return setTimeout(() => console.log("Hola soy una promesa"), 5000);
}

miPromesa();

function* IDPar() {
  let id = 0;
  while (true) {
    yield (id += 2);
  }
}

console.log(IDPar().next().value);
console.log(IDPar().next().value);
console.log(IDPar().next().value);
console.log(IDPar().next().value);
