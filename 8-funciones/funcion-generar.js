function* generarID() {
  let id = 0;
  while (true) {
    id++;
    if (id >= 10) {
      return id;
    }
    yield id; //se va a quedar esperando hasta que se vuelva a llamar
  }
}

const gen = generarID();

console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
