// funciones asincronas
//
function miAsinc() {
  //hace una llamada a una base de datos externa
  ////puede generarnos algun error
  //
}

const miPromesa = new Promise((resolve, reject) => {
  const i = Math.floor(Math.random() * 2);
  //si esta todo corecto
  if (i !== 0) {
    resolve();
  } else {
    reject();
  }
});

miPromesa
  .then(() => console.log("Se ejecuto de forma correcta"))
  .catch(() => console.log("ERROR"))
  .finally(() => console.log("Yo me ejecuto siempre"));
