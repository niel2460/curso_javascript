const logger = require("./logger/");

function mostrarError() {
  throw new error("Mostrando error!");
}

try {
  mostrarError();
} catch (e) {
  logger.error(`Mensaje de error: ${e}`);
} finally {
  console.log("Fin del proceso");
}
