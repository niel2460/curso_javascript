//gestion de errores
const miFuncion = (val) => {
  if (typeof val === "number") {
    return 2 * val;
  }
  throw new Error("El valor debe ser de tipo numero");
};

// const elDoble = miFuncion("ala");

const numero = "8";

try {
  //codigo que puede fallar
  console.log("Esta ejecutandose forma correcta");
  const doble = miFuncion(numero);
  console.log(doble);
} catch (e) {
  //En caso de fallar, quiero que se ejecute esto
  console.log(`el valor de e es:${e}`);
  console.error("ERROR!");
} finally {
  console.log(
    "Esto se va a ejecutar tanto si tenemos un error, como si no existe ninguno"
  );
}

//InternalError, SyntaxError, TypeError, RangeError y ReferenceError
