const logger = require("./logger/");

logger.info("Hola esto es un mensaje informativo");
logger.debug("Hola esto es un mensaje debug");
logger.warn("Hola esto es un mensaje de advertencia");
logger.error("Esto es un error");

// console.log("Hola estoy saliendo en pantalla");
// console.info("Hola esto es un mensaje informativo");
// console.debug("Hola esto es un mensaje debug");
// console.warn("Hola esto es un mensaje de advertencia");
// console.error("Esto es un error");
